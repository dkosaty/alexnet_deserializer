import os, re, numpy as np
from main import unpickle
from scipy import misc

class Deserializer:

    def __init__(self, options):
        self.saving_folder = options.saving_folder

        if options.image_size is None:
            self.image_size = 32
        else:
            self.image_size = options.image_size

        train_batch_names = Deserializer.parse_range(options.dataset_path, options.train_range)
        test_batch_names = Deserializer.parse_range(options.dataset_path, options.test_range)
        self.names_batches_list = {'train': train_batch_names, 'test': test_batch_names}

    @staticmethod
    def parse_range(dataset_path, range_type):
        pattern = r'(\d+)-?(\d*)'
        founded = re.search(pattern, range_type)
        if founded:
            if founded.endpos == 3:
                first, last = int(founded.group(1)), int(founded.group(2))
            else:
                first = int(founded.group(1))
                last = first
            batch_names = [os.path.join(dataset_path, 'data_batch_'+str(num)) for num in range(first, last+1)]
        else:
            raise ValueError, "Don't parse the ranges."
        return batch_names

    def parse(self, dataset_type):
        for filename in self.names_batches_list[dataset_type]:
            raw_data = unpickle(os.path.join(self.saving_folder, filename))

            for i in range(len(raw_data['data'][1])):
                image = self.extract_image(raw_data['data'][:,i])

                label = raw_data['labels'][i]

                # misc.imshow(image)

                misc.imsave(os.path.join(self.saving_folder, dataset_type, str(label), raw_data['filenames'][i]), image)

    def extract_image(self, data):
        format = np.zeros((self.image_size, self.image_size, 3), np.uint8)

        for r in range(self.image_size):
            for c in range(self.image_size):
                k = r+self.image_size*c
                format[r,c] = [data[k], data[k+1024], data[k+2048]]

        return format.T