from optparse import OptionParser
from deserializer import *

def main():
    try:
        options = parse_command_line()

        if not os.path.exists(options.saving_folder):
            os.makedirs(options.saving_folder)

        meta = unpickle(os.path.join(options.dataset_path, 'batches.meta'))

        create_directory(os.path.join(options.saving_folder, 'train'), meta=meta)
        create_directory(os.path.join(options.saving_folder, 'test'), meta=meta)

        parser = Deserializer(options)

        print 'parse the train instances'
        parser.parse('train')

        print 'parse the test instances'
        parser.parse('test')
    except ValueError as error:
        print 'ValueError excepted: ',error.message
    except RuntimeError as error:
        print 'RuntimeError excepted: ',error.message

def unpickle(file):
    import cPickle
    fo = open(file, 'rb')
    dict = cPickle.load(fo)
    fo.close()
    return dict

def create_directory(directory_name, meta):
    if not os.path.exists(directory_name):
        os.makedirs(directory_name)

    for i in range(len(meta['label_names'])):
        folder_name = os.path.join(directory_name, str(i))
        if not os.path.exists(folder_name):
            os.makedirs(folder_name)

def parse_command_line():
    parser = OptionParser()

    parser.add_option('--dataset_path', dest='dataset_path', type='string')
    parser.add_option('--image_size', dest='image_size', type='int')
    parser.add_option('--train_range', dest='train_range', type='string')
    parser.add_option('--test_range', dest='test_range', type='string')
    parser.add_option('--saving_folder', dest='saving_folder', type='string')

    (options, args) = parser.parse_args()

    return options

if __name__ == '__main__':
    main()